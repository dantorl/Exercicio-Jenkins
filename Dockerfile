FROM openjdk:8-jre-alpine
COPY /target/Api-Investimentos-*.jar /APP/Api-Investimentos.jar
WORKDIR /APP/
CMD ["java", "-jar", "Api-Investimentos.jar"]

